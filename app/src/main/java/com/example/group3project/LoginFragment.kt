package com.example.group3project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.group3project.databinding.FragmentLoginBinding
import com.example.group3project.databinding.FragmentMapBinding

class LoginFragment : Fragment() {

    lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(this.layoutInflater, container, false)

        binding.LoginLoginPage.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragment2ToAdminAccounFragment()
            findNavController().navigate(action)
        }

        binding.SignUpLoginPage.setOnClickListener {
            val action = LoginFragmentDirections.actionLoginFragment2ToSignUpFragment()
            findNavController().navigate(action)
        }

        return binding.root
    }
}