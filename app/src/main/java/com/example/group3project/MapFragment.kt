package com.example.group3project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.group3project.databinding.FragmentMapBinding

class MapFragment : Fragment() {

    lateinit var binding:FragmentMapBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMapBinding.inflate(this.layoutInflater, container, false)

        binding.Login.setOnClickListener {
            val action = MapFragmentDirections.actionMapFragmentToLoginFragment2()
            findNavController().navigate(action)
        }

        binding.SignUp.setOnClickListener {
            val action = MapFragmentDirections.actionMapFragmentToSignUpFragment()
            findNavController().navigate(action)
        }

        binding.search.setOnClickListener{
            val action = MapFragmentDirections.actionMapFragmentToNavigationFragment()
            findNavController().navigate(action)
        }

        return binding.root
    }

}