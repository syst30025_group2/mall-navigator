package com.example.group3project.Models

object PedestrianDeadReckoner {
    val currentVector = Vector(Location().delta.sumBy { it.distance }, Location().delta.sumBy { it.angle })
    // The currentVector is our current position relative to our starting point

    fun monitor() {
        while (InertalSensor.active(true)) {
            Location().delta.add(InertalSensor.data())
        }
    }
}