package com.example.group3project.Models

data class Vector (val distance: Int, val angle: Int)