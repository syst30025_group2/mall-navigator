package com.example.group3project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.group3project.databinding.FragmentMapBinding
import com.example.group3project.databinding.FragmentSignUpBinding

class SignUpFragment : Fragment() {

    lateinit var binding: FragmentSignUpBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSignUpBinding.inflate(this.layoutInflater, container, false)

        binding.CreateAccount.setOnClickListener {
            val action = SignUpFragmentDirections.actionSignUpFragmentToLoginFragment2()
            findNavController().navigate(action)
        }


        return binding.root
    }
}